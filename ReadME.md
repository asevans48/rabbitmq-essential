## RabbitMQ Essentials ##

Code repository containing examples from the book *Rabbit MQ Essentials Volume 2.* Code is available from the folder
corresponding to the chapter name. This repository includes tests and further documentation. 

# RabbitMQ Setup

The book uses the docker setup similar to that defined in the [Docker](https://www.docker.com/) compose file. You shuold
review the file before running *docker-compose up.* There are default usernames and passwords in this file used to
connect to the broker. You should change these defaults as they are easy to guess if testing in an insecure space. 